# Getting Python on MacOS or Linux

Python 2.7 comes bundled with MacOS and Linux. However, we will be using Python 3, so make sure that you install Python3.

If you get python up and running, check out this introductory prep lecture on Python that was prepared for a Fullstack IoT bootcamp course: [link](http://www.youtube.com/watch?v=HWDE5m9aT-8). This video assumes you have some previous MATLAB or undergraduat engineering experience in programming.

### MacOS

On MacOS, we can use [homebrew](https://brew.sh) to install python 3. Open a terminal and type:

```
brew install python
```

This will install python3. To verify you have the latest installed, in a terminal type:

```
python --version
```

You should see 3.7.

> If you have problems, consult [this link](https://docs.python-guide.org/starting/install3/osx/) for more information

### Linux (Debian)

In a terminal:

```
sudo apt-get update
sudo apt-get install python3.7
```

You will need to use the `python3` and `pip3` command in the place of the `python` command if you leave your path in the default configuration.

# Setting Up Python3 in Windows

For an in-depth walk-through, review the video: [Video Link](https://vimeo.com/350013920)

## Steps/Links

1. Download Python [link](https://www.python.org/ftp/python/3.7.4/python-3.7.4.exe)
2. Install Python - Make sure to check the `Set Python on Path` in the first installation screen.
3. Verify the python installation by opening a command window and typing `python`

> Troubleshooting: If python cannot be found, be sure to watch the end of the video to set your Windows path
