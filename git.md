## Getting and Using Git

If you are more comfortable using a user interface, you may consider downloading GitHub's [desktop client](https://desktop.github.com).

> We are not using Github (we are using Gitlab). While it is a good idea to have a github account, you do not need to enter it into the UI. Simply skip the username entry questions, and select the `URL` option for cloning a repository.

Git is a powerful open source version control and collaboration software tool that enables developers to version and collaborate on code and documents.

MacOS and Linux come with `git` included on the command line.

> To install the windows command line version, [download this](https://github.com/git-for-windows/git/releases/download/v2.22.0.windows.1/Git-2.22.0-64-bit.exe) executable and install it with all the default options. Once installed, it will open up a shell that will allow you to run git commands.

To get started, open a terminal and type:

```
git clone https://gitlab.com/systems-iot/resources-and-tools.git
```

This will `clone` this repository and all the files. You should see a number of `.md` (markdown) files in a folder called `resources-and-tools` where you cloned the repository.

The act of cloning a repository makes a local copy of the repository that you can modify and edit.

## Making your own repository on Gitlab

You will want to make your own repository that you can edit and share when working with a team. We will use [gitlab.com](https://gitlab.com) to make a repository.

First, register for an account and create a new repository (project) called `first-repository`. Once you have created a project, copy the URL and open a terminal to clone your newly created repository:

```
git clone https://gitlab.com/[your-username]/first-repository.git
cd first-repository
```

Once you are in the newly cloned folder, create a file in that folder called `README.md`. Open `README.md` in your favorite text editor and type "This is my first markdown". Save and close the file.

In your terminal, type

```
git add .
```

This will add any newly created or modified files (like your `README.md`) to your local git tracked file listing. Next, you will want to commit any changes to your local git with a message:

```
git commit -m "readme file"
```

At this point, we have not pushed anything to the remote repository (gitlab) yet. We have simply staged our files to be ready for pushing. Once we are satisfied that our files have been commited (you can verify this with a `git status`), push your files to your remote (gitlab) repository:

```
git push origin master
```

This will push the files to gitlab under the `master` branch. You can read more about git branching [here](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging) You can visit the repository in a browser to see your latest files!

## Using Git in a team

Often times, if you are using `git` in a team, you will need to pull code from the `master` branch that someone else has pushed changes to. Before pulling the changes, make sure your changes (if any) are committed (but not pushed) by doing a `git commit -m "my message` first. Then do a pull to get the lastest:

```
git pull origin master
```

This will get the latest changes from the remote repository.

> If you do not care about keeping your changes since your last pull or push, you can do `git stash` which will stash your changes and return your current branch to the last push or pull.

## Finishing up

Git is an extremely powerful tool, and we have not even scratched the surface of what it can do. It can also be extremely frustrating at times. If you keep in mind where your remote and local repository is in terms of changes, you should avoid many of the problems that beginners tend to struggle with.
