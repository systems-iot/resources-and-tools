# Get VS Code

VS Code has recently become the defacto standard for code editing. Also known as an IDE (integrated Desktop Environment), VS Code provides a robust platform for editing and running code.

To download, visit the [VSCode Website](https://code.visualstudio.com/download) to get the version for your platform of choice.

You may want to review [developing Python in VS Code](https://code.visualstudio.com/docs/languages/python)

### A note about Running Python in VS Code on Windows

If you are on Windows (and MacOS, too), we highly recommend installing the VS Code Python extension, which provides you the ability to run python code with a right click option on the menu within VS Code. You can learn about VS code extensions [here](vs code extensions). The python extension is [this one](https://marketplace.visualstudio.com/items?itemName=ms-python.python).
