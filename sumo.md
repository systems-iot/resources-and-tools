# Getting SUMO

SUMO is a microscopic traffic system simulation software package that is available for MacOS, Linux and Windows plaforms.

For introduction to SUMO and TraCI, check out the [repo and videos](https://gitlab.com/systems-iot/sumo-primer). Videos:

1. [Getting Started](https://vimeo.com/350015032)
2. [Controlling SUMO with TraCI and Python](https://vimeo.com/350015321)

For official install documentation on installation, check out the [SUMO docs](https://sumo.dlr.de/wiki/Installing).

## Windows

Installing SUMO on Windows is simple. Download the Windows binary [here](http://sumo.sourceforge.net/userdoc/Downloads.html#MS_Windows_binaries).

When installing, make sure to select the `Allow SUMO_HOME and PYTHONPATH to be set`. This will put SUMO on your path so you can run SUMO directly from the command line with `sumo-gui` command. In addition, it makes integrating with TraCI easier.

## MacOS

To install SUMO on MacOS, `brew` is recommend:

```
brew update
brew tap dlr-ts/sumo
brew install sumo
```

In addition, you will need `xquartz` as an X11 window manager:

```
brew install xquartz
```

> Make sure to have xquartz running when trying to start SUMO!

## Linux (Debian)

Simply install using `apt-get`

```
sudo apt-get install sumo sumo-tools sumo-doc
```
