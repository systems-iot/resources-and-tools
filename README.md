# Resources and Tools for System IoT

### Review the [introduction video here.](https://vimeo.com/350773760)

The following are the tools that are used in the introduction to systems. While it may seem that these implementation examples and concepts are extremely software-heavy, most (if not all) of todays engineering systems are run, managed, and tied together with software. Whether you go into technical implementation or management within systms enginering, you will come in contact with all the concepts that are covered below in one way or another.

One of the more interesting system topics is Internet of Things (IoT). IoT encompasses a significant number of systems, and spans industries and technologies.

The examples below use a traffic system as an example in which multiple systems (vehicles, infrastucture, and logistics) are tied together via IoT using a managed IoT service (Iotery). To model a traffic system, we will use SUMO, a microscopic traffic simulation software package.

The primary tools/concepts that will be needed:

1. Git - [link](https://gitlab.com/systems-iot/resources-and-tools/blob/master/git.md)
2. Python -[link](https://gitlab.com/systems-iot/resources-and-tools/blob/master/python.md)
3. Heroku - [link](https://devcenter.heroku.com/articles/heroku-cli)
4. Iotery - [link](https://dev.to/bjyurkovich/get-started-with-your-iot-devices-using-iotery-io-4c2d) | [video](https://gitlab.com/systems-iot/iotery-basics-walkthrough.git)
5. SUMO (and TraCI) - [link](https://gitlab.com/systems-iot/resources-and-tools/blob/master/sumo.md)
6. Visual Code - [link](https://gitlab.com/systems-iot/resources-and-tools/blob/master/vscode.md)
7. Implementation of API - [link](https://gitlab.com/systems-iot/api-server-implementation)

It is highly suggested to review all the material above, and even take a stab at doing some of the things before the lecture. There is a lot to cover, and a single 3 hour lecture will not provide enough depth to accomplish your assignments and project!

## Video Index Reference:

- Using git in your assignments - [link](https://vimeo.com/350971946)
- Install Python in Windows - [link](https://vimeo.com/350013920)
- Python Intro - [link](http://www.youtube.com/watch?v=HWDE5m9aT-8)
- Getting Started with SUMO - [link](https://vimeo.com/350015032)
- Controlling SUMO with Python - [link](https://vimeo.com/350015321)
- Implementing API Server for System Communication - [link](https://vimeo.com/350646693)

### Helpful supporting video material

The following videos are part of a fullstack systems engineering course provided by Technicity, a software development company. The videos are preparation videos and address the very basics of concepts needed.

- Using the command line on MacOS and Linux - [link](https://www.youtube.com/watch?v=nAkF9Ss7vRc&feature=youtu.be)
- More on Git - [link](https://www.youtube.com/watch?v=cjQDk86ndow&feature=youtu.be)
- Basics of Networking - [link](https://www.youtube.com/watch?v=8i5RB9vXIE4&feature=youtu.be)
